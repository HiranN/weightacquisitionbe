//In the service we are able to access to the entity through the Repository object
package com.WeightAcquisition.WeightAcquisition.Service;

import com.WeightAcquisition.WeightAcquisition.DAO.WeightAcquisitionSimplified;
import com.WeightAcquisition.WeightAcquisition.Repository.WeightAcquisitionRepo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class WeightAcquisitionService {

    @Autowired
    private WeightAcquisitionRepo weightAcquisitionRepo;




    public List<WeightAcquisitionSimplified> fetchTop10()
    {
        return weightAcquisitionRepo.findTop10Records();
    }

    public List<WeightAcquisitionSimplified> getSimplifiedVersion()
    {
        List<WeightAcquisitionSimplified> entities = weightAcquisitionRepo.findAll();
        List<WeightAcquisitionSimplified> daos = entities.stream()
                .map(entity -> {
                    WeightAcquisitionSimplified dao = new WeightAcquisitionSimplified();
                    dao.setId(entity.getId());
                    dao.setWeight(entity.getWeight());
                    dao.setVelocity(entity.getVelocity());

                    return dao;
                })

                .collect(Collectors.toList());


                return daos;
    }


}
