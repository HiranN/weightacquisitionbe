//We can't access the entity directly, only with the help of the repo we will be able to access the functions and data that are inside WeightAcquisition Entity
package com.WeightAcquisition.WeightAcquisition.Repository;

import com.WeightAcquisition.WeightAcquisition.DAO.WeightAcquisitionSimplified;
import com.WeightAcquisition.WeightAcquisition.Entity.WeightAcquisition;
import com.WeightAcquisition.WeightAcquisition.Entity.WeightAcquisitionId;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface WeightAcquisitionRepo extends JpaRepository<WeightAcquisitionSimplified, WeightAcquisitionId> {

    @Query(value = "SELECT * FROM spaa.tspa01_wei_acq s LIMIT 10" , nativeQuery = true)
    public List<WeightAcquisitionSimplified> findTop10Records();

    //@Query(value = "SELECT s FROM WeightAcquisition s where name=:val and mark=:m")
    //public WeightAcquisition fetchUsingName(@Param("val")String name, @Param("m") int maark);

    //@Query(value = "SELECT s FROM WeightAcquisition s where mark=:m")
    //public WeightAcquisition fetchUsingMark(@Param("m") int maark);

}
