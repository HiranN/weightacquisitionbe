package com.WeightAcquisition.WeightAcquisition.Entity;

import jakarta.persistence.*;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.sql.Timestamp;

@Entity
@Data
@Table(name = "tspa01_wei_acq")
@NoArgsConstructor
@AllArgsConstructor
public class WeightAcquisition {

    @EmbeddedId
    private WeightAcquisitionId id;

    @Column(name = "n_wei_ton")
    private Double weight;

    @Column(name = "s_msr_tim_utc", nullable = false)
    private String utc;

    @Column(name = "q_vel_kmh")
    private Double velocity;

    @Column(name = "n_dir")
    private Integer direction;

    @Column(name = "q_len_m")
    private Double length;

    @Column(name = "n_vcl_cls")
    private String vehicle;

    @Column(name = "n_err_msg")
    private String error;

    @Column(name = "n_wrn_msg")
    private String warning;

    @Column(name = "t_axl_dta")
    private String AxlData;

    @Column(name = "b_clb")
    private String Clb;

    @Column(name = "d_ins")
    private Timestamp timestamp;

    @Column(name = "n_axl_num")
    private Integer AxlNum;

    public WeightAcquisitionId getId() {
        return id;
    }

    public Double getWeight() {
        return weight;
    }

    public String getUtc() {
        return utc;
    }

    public Double getVelocity() {
        return velocity;
    }

    public Integer getDirection() {
        return direction;
    }

    public Double getLength() {
        return length;
    }

    public String getVehicle() {
        return vehicle;
    }

    public String getError() {
        return error;
    }

    public String getWarning() {
        return warning;
    }

    public String getAxlData() {
        return AxlData;
    }

    public String getClb() {
        return Clb;
    }

    public Timestamp getTimestamp() {
        return timestamp;
    }

    public Integer getAxlNum() {
        return AxlNum;
    }
}
