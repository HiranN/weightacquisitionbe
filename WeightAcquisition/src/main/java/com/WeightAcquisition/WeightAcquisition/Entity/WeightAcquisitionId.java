package com.WeightAcquisition.WeightAcquisition.Entity;

import jakarta.persistence.Column;
import jakarta.persistence.Embeddable;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

@Data
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
public class WeightAcquisitionId implements Serializable {

    @Column(name = "t_dev_id", nullable = false)
    private String devId;

    @Column(name = "n_msr_tim_ms", nullable = false)
    private Long time;

    @Column(name = "n_lne", nullable = false)
    private Integer lane;
}
