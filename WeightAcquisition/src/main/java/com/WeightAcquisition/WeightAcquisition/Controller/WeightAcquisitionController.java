package com.WeightAcquisition.WeightAcquisition.Controller;

import com.WeightAcquisition.WeightAcquisition.DAO.WeightAcquisitionSimplified;
import com.WeightAcquisition.WeightAcquisition.Entity.WeightAcquisition;
import com.WeightAcquisition.WeightAcquisition.Service.WeightAcquisitionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@CrossOrigin(allowedHeaders = "*", origins = "*")
public class WeightAcquisitionController {

    @Autowired
    private WeightAcquisitionService weightAcquisitionService;

    @GetMapping("/customFetch")
    public List<WeightAcquisitionSimplified> fetchTop3Controller (){
        return weightAcquisitionService.fetchTop10();
    }

    @GetMapping("/FetchDao")
    public List<WeightAcquisitionSimplified> getSimplifiedVersion() {
        return weightAcquisitionService.getSimplifiedVersion();
    }



}
