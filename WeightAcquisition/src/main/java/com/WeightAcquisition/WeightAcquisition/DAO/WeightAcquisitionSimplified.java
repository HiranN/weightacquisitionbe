package com.WeightAcquisition.WeightAcquisition.DAO;


import com.WeightAcquisition.WeightAcquisition.Entity.WeightAcquisitionId;
import jakarta.persistence.Column;
import jakarta.persistence.EmbeddedId;
import jakarta.persistence.Entity;

@Entity
public class WeightAcquisitionSimplified {

    @EmbeddedId
    private WeightAcquisitionId id;

    @Column(name = "n_wei_ton")
    private Double weight;

    @Column(name = "q_vel_kmh")
    private Double velocity;

    public WeightAcquisitionSimplified(){

    }

    public WeightAcquisitionSimplified(WeightAcquisitionId id, Double weight, Double velocity) {
        this.id = id;
        this.weight = weight;
        this.velocity = velocity;
    }

    public WeightAcquisitionId getId() {
        return id;
    }

    public void setId(WeightAcquisitionId id) {
        this.id = id;
    }

    public Double getWeight() {
        return weight;
    }

    public void setWeight(Double weight) {
        this.weight = weight;
    }

    public Double getVelocity() {
        return velocity;
    }

    public void setVelocity(Double velocity) {
        this.velocity = velocity;
    }
}
